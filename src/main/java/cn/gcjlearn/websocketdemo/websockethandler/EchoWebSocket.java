package cn.gcjlearn.websocketdemo.websockethandler;
import org.springframework.stereotype.Component;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@Component
@ServerEndpoint("/echo")
public class EchoWebSocket {
    private String username;
    @OnOpen
    public void open(Session session) throws IOException {
        String queryString = session.getQueryString();
        String[] split = queryString.split("=");
        username = split[1];
        session.getBasicRemote().sendText("欢迎进入聊天室："+username);
    }

    @OnMessage
    public void message(Session session, String msg) throws IOException {
        session.getBasicRemote().sendText(username+"说："+msg);

    }

    @OnClose
    public void close(Session session){}
}
