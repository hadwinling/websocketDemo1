package cn.gcjlearn.websocketdemo.vo;

import java.util.Date;
import java.util.List;

public class JsonResultWebSocket {
    /*
    * 0：欢迎，欢迎也许返回当前在线人数及性名
    * 1：通知
    * 2：发送消息
    * */
    private Integer type;
    private String from;
    private String to;
    private String content;
    private List<String> usernames;
    private Date date;

    @Override
    public String toString() {
        return "JsonResultWebSocket{" +
                "type='" + type + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                '}';
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
