package cn.gcjlearn.websocketdemo.vo;
import java.util.Arrays;

public class SendMessage {
    private String[] to;
    private String msg;
    private Integer type;

    public String[] getTo() {
        return to;
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SendMessage{" +
                "to=" + Arrays.toString(to) +
                ", msg='" + msg + '\'' +
                ", type=" + type +
                '}';
    }
}
