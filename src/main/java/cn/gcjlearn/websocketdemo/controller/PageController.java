package cn.gcjlearn.websocketdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {
    @RequestMapping("Echo")
    public String index(){
        return "Echo";
    }

    @RequestMapping("Login")
    public String login(){
        return "/pages/login";
    }


    @RequestMapping("Chat")
    public String chat(){
        return "Chat";
    }

    @RequestMapping("/{pagePath}")
    public String toPage(@PathVariable("pagePath") String path){
        System.out.println(path);
        return null;
    }
}
