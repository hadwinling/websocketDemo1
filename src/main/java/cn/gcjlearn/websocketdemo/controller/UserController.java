package cn.gcjlearn.websocketdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("user")
public class UserController {

    @RequestMapping("login.do")
    public ModelAndView login(String username, Model model){
        ModelAndView mav = new ModelAndView();
        mav.addObject("username",username);
        mav.setViewName("Chat");
        return mav;
    }
}
