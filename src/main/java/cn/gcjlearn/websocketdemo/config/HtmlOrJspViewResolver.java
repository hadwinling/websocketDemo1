package cn.gcjlearn.websocketdemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

public class HtmlOrJspViewResolver {
    @Value("${spring.mvc.view.prefix}")
    private String prefix = "";

    @Value("${spring.mvc.view.suffix}")
    private String suffix = "";

    @Value("spring.mvc.view.view-name")
    private String viewNames= "";

    @Bean
    InternalResourceViewResolver jspViewResolver(){
        final InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix(prefix);
        viewResolver.setSuffix(suffix);
        viewResolver.setViewClass(JstlView.class);
        System.out.println(viewNames);
        viewResolver.setViewNames(viewNames);
        return viewResolver;
    }
}
