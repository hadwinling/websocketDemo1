<%--
  Created by IntelliJ IDEA.
  User: Gongcj
  Date: 06-28-2019
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Echo</title>
</head>
    <body>
        <div id="sse">
            <a href="javascript:WebSocketTest()">运行 WebSocket</a>
        </div>
        <input type="text" id="msg">
        <button onclick="sendMsg()">发送</button>
    <div id="dv"></div>
    </body>
    <script type="text/javascript">
        var ws;
        window.onload = function()
        {
            if ("WebSocket" in window)
            {
                alert("您的浏览器支持 WebSocket!");
                username = '${username}';
                // 打开一个 web socket
                ws = new WebSocket("ws://127.0.0.1:8080/echo?username="+username);


                ws.onmessage = function (evt)
                {
                    var received_msg = evt.data;
                    document.getElementById("dv").innerHTML += received_msg+"<br>";
                };

                ws.onclose = function()
                {
                    // 关闭 websocket
                    alert("连接已关闭...");
                };
            }

            else
            {
                // 浏览器不支持 WebSocket
                alert("您的浏览器不支持 WebSocket!");
            }
        }
        function sendMsg(){
            msg = document.getElementById("msg").value;
            ws.send(msg);
            document.getElementById("msg").value ="";
        }
    </script>
</html>
